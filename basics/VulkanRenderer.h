#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>

#include <stdexcept>
#include <vector>
#include <set>
#include <algorithm>
#include <array>

#include "Utilities.h"
#include "Mesh.h"

class VulkanRenderer
{
public:
	VulkanRenderer();

	int init(GLFWwindow* newWindow);

	void updateModel(int modelId,glm::mat4 newModel);
	
	void draw();
	void cleanup();
	
	~VulkanRenderer();

private:
	GLFWwindow* m_window;

	int m_currentFrame = 0;

	// Scene Objects
	std::vector<Mesh> m_meshList;

	// Scene settings
	struct UboViewProjection
	{
		glm::mat4 projection;
		glm::mat4 view;	
	} uboViewProjection;
	
	// Vulkan Components
	// - Main
	VkInstance m_instance;
	struct 
	{
		VkPhysicalDevice m_physicalDevice;
		VkDevice m_logicalDevice;
	} m_mainDevice;
	VkQueue m_graphicsQueue;
	VkQueue m_presentationQueue;
	VkSurfaceKHR m_surface;
	VkSwapchainKHR m_swapChain;

	// 1:1 connected these 3
	std::vector<SwapChainImage> m_swapChainImages;
	std::vector<VkFramebuffer> m_swapChainFrameBuffers;
	std::vector<VkCommandBuffer> m_commandBuffers;

	VkImage m_depthBufferImage;
	VkDeviceMemory m_depthBufferImageMemory;
	VkImageView m_depthBufferImageView;
	VkFormat m_depthFormat;

	// DESCRIPTORS
	VkDescriptorSetLayout m_descriptorSetLayout;
	VkPushConstantRange m_pushConstantRange;
	
	VkDescriptorPool m_descriptorPool;
	std::vector<VkDescriptorSet> m_descriptorSets;
	
	std::vector<VkBuffer> m_vpUniformBuffer;
	std::vector<VkDeviceMemory> m_vpUniformBufferMemory;

	std::vector<VkBuffer> m_modelDUniformBuffer;
	std::vector<VkDeviceMemory> m_modelDUniformBufferMemory;

	// VkDeviceSize m_minUniformBufferOffset;
	// size_t m_modelUniformAlignment;
	// Model* m_modelTransferSpace;
	
	// Pipeline
	VkPipeline m_graphicsPipeline;
	VkPipelineLayout m_pipelineLayout;
	VkRenderPass m_renderPass;

	// Command Pool
	VkCommandPool m_graphicsCommandPool;

	// - Utility
	VkFormat m_swapChainImageFormat;
	VkExtent2D m_swapChainExtent;


	// -- SYNCHRONISATION --
	std::vector<VkSemaphore> m_imageAvailable;
	std::vector<VkSemaphore> m_renderFinished;
	std::vector<VkFence> m_drawFences;

	// Vulkan Functions
	// - Create Functions
	void createInstance();
	void createLogicalDevice();
	void createSurface();
	void createSwapChain();
	void createRenderPass();
	void createDescriptorSetLayout();
	void createPushConstantRange();
	void createGraphicsPipeline();
	void createDepthBufferImage();
	void createFrameBuffers();
	void createCommandPool();
	void createCommandBuffers();
	void createSynchronisation();

	void createUniformBuffers();
	void createDescriptorPool();
	void createDescriptorSets();

	void updateUniformBuffers(uint32_t imageIndex);
	

	// - Record Functions
	void recordCommands(uint32_t currentImage);

	// - Get Functions
	void getPhysicalDevice();

	// - Allocate Functions
	void allocateDynamicBufferTransferSpace();

	// - Support Functions
	// -- Checker Functions
	bool checkInstanceExtensionSupport(std::vector<const char*>* checkExtensions);
	bool checkDeviceExtensionSupport(VkPhysicalDevice device);
	bool checkDeviceSuitable(VkPhysicalDevice m_physicalDevice);

	// -- Getter Functions
	QueueFamilyIndicies getQueueFamilies(VkPhysicalDevice m_physicalDevice);
	SwapChainDetails getSwapChainDetails(VkPhysicalDevice device);

	// -- Choose functions
	VkSurfaceFormatKHR chooseBestSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& formats);
	VkPresentModeKHR chooseBestPresentationMode(const std::vector<VkPresentModeKHR> presentationModes);
	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& surfaceCapabilities);
	VkFormat chooseSupportedFormat(const std::vector<VkFormat> &formats, VkImageTiling tiling, 
		VkFormatFeatureFlags featureFlags);

	// -- Create Functions
	VkImage createImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling,
	VkImageUsageFlags useFlags, VkMemoryPropertyFlags propFlags, VkDeviceMemory* imageMemory);
	
	VkImageView createImageView(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags);
	VkShaderModule createShaderModule(const std::vector<char>& code);
	
	
};

